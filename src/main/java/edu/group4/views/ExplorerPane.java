package edu.group4.views;

import edu.group4.controllers.MainController;
import edu.group4.model.ImageDetails;
import edu.group4.model.MetaImage;
import edu.group4.model.MetaImageDB;
import javafx.scene.CacheHint;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;


/**
 * This class handles everything that does with displaying and storing a processed image.
 * Its the middle of the application, the "Browse" pane.
 * <p>
 * Each image is stored in a TreeMap to make sure each image is
 * only processed once, and that the StackPane is associated with
 * the corresponding id to the image stored inside.
 * <p>
 * We decided to store all necessary information about an image in the application itself to
 * limit the use of database connections. The database is only accessed
 * when an image or label is added/removed.
 * <p>
 * If you search after an image, you search through all the images stored in
 * the application instead of the database.
 */
public class ExplorerPane {

    private static ExplorerPane explorerPane_instance = null;
    private final MainController mainController;
    private final ScrollPane explorerPane;
    private final FlowPane flowPane;
    private final TreeMap<String, StackPane> imageViews;
    private final TreeMap<String, ImageDetails> imagesFromDB;
    private final ArrayList<String> selectedImages;
    private final MetaImage metaImage;
    private boolean searchActive = false;
    private boolean singleSelect = true;


    /**
     * Creates an instance of the explorerPane.
     */
    private ExplorerPane() {
        this.mainController = new MainController();
        this.imageViews = new TreeMap<>();
        this.selectedImages = new ArrayList<>();
        this.imagesFromDB = new TreeMap<>();
        this.metaImage = MetaImageDB.getInstance();
        this.explorerPane = new ScrollPane();
        this.explorerPane.setId("explorerPane");
        this.explorerPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        this.flowPane = new FlowPane();
        this.flowPane.prefWidthProperty().bind(explorerPane.widthProperty());
        this.flowPane.setId("explorerPane");

        this.explorerPane.setContent(flowPane);
        ImageViewPane.getInstance().viewSelectedImage(null, getSelectedImages(), null, null, true);
    }

    /**
     * Returns an instance of ExplorerPane.
     * Its a Lazy initialization
     *
     * @return instance of ExplorerPane
     */
    public static ExplorerPane getInstance() {
        if (explorerPane_instance == null) explorerPane_instance = new ExplorerPane();
        return explorerPane_instance;
    }

    /**
     * Returns the ScrollPane
     *
     * @return ScrollPane displaying the features.
     */
    public ScrollPane getPane() {
        return explorerPane;
    }

    /**
     * Creates an imageView with the added image.
     * Scales the image down to 300px wide, but preserves ratio.
     * Smoothing is disabled to achieve less possessing.
     * Add the image to allImages and display the image if search is not active.
     * You can double click to view image
     *
     * @param imageDetails new image
     */
    public void addImage(ImageDetails imageDetails) {
        Image newImage = new Image(Paths.get(imageDetails.getPath()).toUri().toString(), 300, 0, true, false);
        ImageView iv = new ImageView(newImage);
        if (newImage.getWidth() > newImage.getHeight()) iv.setFitWidth(125);
        else iv.setFitHeight(125);
        iv.setPreserveRatio(true);
        iv.setSmooth(false);
        iv.setCache(true);
        iv.setCacheHint(CacheHint.SPEED);

        // Add a box around the image view.
        // Makes it possible to scale, set border-color, highlight etc.
        StackPane pane = new StackPane();
        pane.getStyleClass().add("ExplorerPaneImg");
        pane.getChildren().add(iv);
        pane.setOnMouseClicked(e -> {
            if (e.getButton().equals(MouseButton.PRIMARY)) {
                if (e.getClickCount() == 2) {
                    this.mainController.imagePopOut(imageDetails.getPath(), this.imagesFromDB);
                    this.singleSelect = true;
                } else
                    this.mainController.imageClicked(pane, imageDetails.getPath(), this, ImageViewPane.getInstance());
            }
        });
        addRightClickMenu(imageDetails.getPath(), pane);

        // Add the pane with image to the TreeMap
        this.imageViews.put(imageDetails.getPath(), pane);
        this.imagesFromDB.put(imageDetails.getPath(), imageDetails);

        // Add the new image if search is not active
        if (!searchActive) this.flowPane.getChildren().add(pane);
    }

    /**
     * Display all images depending on input
     *
     * @param imageList list of image ids
     */
    public void displayImages(List<String> imageList) {
        this.flowPane.getChildren().clear();
        imageList.forEach(imageID -> this.flowPane.getChildren().add(this.imageViews.get(imageID)));
    }

    /**
     * Get all the images that is imported from the database
     *
     * @return A map of all the images.
     */
    public TreeMap<String, ImageDetails> getImagesFromDB() {
        return imagesFromDB;
    }

    /**
     * Get the selected images
     *
     * @return Selected images
     */
    public ArrayList<String> getSelectedImages() {
        return selectedImages;
    }

    /**
     * Gets the flowPane with all the images displayed
     *
     * @return FlowPane with imageView of all the images displayed
     */
    public FlowPane getFlowPane() {
        return flowPane;
    }

    /**
     * Get all the processed images.
     * The reason each image is stored with their ID is to limit the amount of processing.
     * To scale an image multiple times is not necessary.
     *
     * @return all the processed images
     */
    public TreeMap<String, StackPane> getImageViews() {
        return imageViews;
    }

    /**
     * Get if search is active
     *
     * @return <code>True</code> search is active <br>
     * <code>False</code> search is not active
     */
    public boolean isSearchActive() {
        return searchActive;
    }

    /**
     * Set searchActive field.
     *
     * @param searchActive <code>True</code> search is active <br>
     *                     <code>False</code> search is not active
     */
    public void setSearchActive(boolean searchActive) {
        this.searchActive = searchActive;
    }

    /**
     * Get if single select is enabled
     *
     * @return <code>True</code> only one image can be selected <br>
     * <code>false</code> multiple images can beselected
     */
    public boolean isSingleSelect() {
        return singleSelect;
    }


    /**
     * Modify single select parameter
     *
     * @param singleSelect <code>True</code> only one image can be selected <br>
     *                     <code>false</code> multiple images can beselected
     */
    public void setSingleSelect(boolean singleSelect) {
        this.singleSelect = singleSelect;
    }

    /**
     * Opens a menu when right clicking an image.
     *
     * @param id The ID of the image you either pop out or add label to.
     * @param stackPane the stack pane you have to right click for the menu to open.
     */
    private void addRightClickMenu(String id, StackPane stackPane) {
        ContextMenu contextMenu = new ContextMenu();

        MenuItem viewImage = new MenuItem("View Image");
        viewImage.setOnAction(event -> this.mainController.imagePopOut(id, this.imagesFromDB));
        MenuItem addLabel = new MenuItem("Labels");
        addLabel.setOnAction(event -> this.mainController.addLabel(id, this.imagesFromDB, this.metaImage));
        MenuItem delSelectedImage = new MenuItem("Delete selected images");
        delSelectedImage.setOnAction(event -> this.mainController.removeSelectedImages(this.metaImage, this));

        // Add MenuItem to ContextMenu
        contextMenu.getItems().addAll(addLabel, delSelectedImage, viewImage);

        // When user right-click on image
        // TODO: Fix position of menu
        stackPane.setOnContextMenuRequested(event -> contextMenu.show(stackPane, event.getScreenX(), event.getScreenY()));
    }
}