package edu.group4.views;

import edu.group4.controllers.MainController;
import edu.group4.model.MetaImage;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.logging.Logger;

/**
 * This class handles all the different feature panes,
 * like: MetaImage, Import, Create amd Search.
 * It will display the correct feature pane depending on which button is clicked.
 */
public class FeaturePane {
    private static FeaturePane featurePane_instance = null;
    private final Logger logger;
    private final ScrollPane featurePane;
    private final ExplorerPane explorerPane;
    private final MetaImage metaImage;
    private final MainController controller;
    private final VBox fPane;
    private final Label title;
    private final Separator separator;

    /**
     * Initializes the feature pane and load the default tab, MetaImage.
     */
    private FeaturePane() {
        this.logger = Logger.getLogger(MetaImageApp.class.getName());
        this.controller = new MainController();
        this.explorerPane = ExplorerPane.getInstance();
        this.metaImage = this.controller.loadMetaImage();
        this.featurePane = new ScrollPane();
        this.featurePane.setId("featurePane");
        this.featurePane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        this.fPane = new VBox();
        this.fPane.setId("fPane");
        this.fPane.prefWidthProperty().bind(featurePane.widthProperty());

        //all the feature panes have this
        this.title = new Label();
        this.title.setId("featureTitle");
        this.separator = new Separator();

        //Load first tab
        loadMetaImage();
    }

    /**
     * Singleton object because we only want one feature pane.
     *
     * @return the featurePane instance.
     */
    public static FeaturePane getInstance() {
        if (featurePane_instance == null) featurePane_instance = new FeaturePane();
        return featurePane_instance;
    }

    /**
     * Getter for the feature Pane
     *
     * @return Feature Pane
     */
    public ScrollPane getPane() {
        return this.featurePane;
    }

    /**
     * Update the pane when button is clicked
     *
     * @param pane pane
     */
    private void updatePane(Pane pane) {
        featurePane.setContent(pane);
    }

    /**
     * Loads the MetaImage tab with its content.
     */
    public void loadMetaImage() {
        this.title.setText("MetaImage");
        Text infoText = new Text();

        infoText.setId("featureText");
        infoText.wrappingWidthProperty().bind(fPane.widthProperty().subtract(22));
        infoText.fontProperty().setValue(Font.font(14));
        infoText.setText("Welcome to MetaImage\n" +
                "Here you can add and compile all your pictures in one nead place.\n\n" +
                "Import lets you select single images or entire folders and add them to your collection here.\n\n" +
                "You can search through your collection, and organise it neatly by simply right clicking and adding tags to your pictures as you want.\n\n" +
                "Create lets you compile and organise your images into spesific folders, or create gallerys and albums for you to share with friends and family.\n");

        this.fPane.getChildren().clear();
        this.fPane.getChildren().addAll(this.title, this.separator, infoText);
        updatePane(fPane);
    }

    /**
     * Loads the Import tab with its content.
     * Add image, add multiple images
     */
    public void loadImport() {
        this.title.setText("Import");

        GridPane grid = new GridPane();
        grid.setId("fPane");

        Button btnImportFile = new Button("File...");
        Button btnImportFiles = new Button("Multiple Files...");

        btnImportFile.setOnAction(e -> {
            try {
                this.controller.importNewFile("single", metaImage, explorerPane);
            } catch (Exception exception) {
                this.logger.info(exception.getMessage());
            }
        });
        btnImportFiles.setOnAction(e -> {
            try {
                this.controller.importNewFile("multiple", metaImage, explorerPane);
            } catch (Exception exception) {
                this.logger.info(exception.getMessage());
            }
        });


        grid.add(btnImportFile, 0, 0);
        grid.add(btnImportFiles, 0, 1);
        this.fPane.getChildren().clear();
        this.fPane.getChildren().addAll(this.title, this.separator, grid);
        updatePane(this.fPane);
    }

    /**
     * Loads the Create tab with its content:
     * - Create album
     */
    public void loadCreate() {
        this.title.setText("Create");

        Text createText = new Text();
        createText.setId("featureText");
        createText.wrappingWidthProperty().bind(this.fPane.widthProperty().subtract(8));
        createText.setText("To create an album, select the pictures you want in the explorer, and click the button below.");
        Button btnCreate = new Button("Create Album");

        //btnCreate.setOnAction(jpg.createPDF());
        btnCreate.setOnAction(e -> this.controller.createPDF(this.explorerPane.getSelectedImages()));

        this.fPane.getChildren().clear();
        this.fPane.getChildren().addAll(this.title, this.separator, createText, btnCreate);
        updatePane(fPane);
    }

    /**
     * Loads the Search tab with its content.
     * Search after pictures by different parameters.
     * Parameters: Everything, File name, Label and Date.
     */
    public void loadSearch() {
        //Search global variables
        TextField searchField;

        // Search layout
        title.setText("Search");

        HBox firstLayer = new HBox();
        firstLayer.setId("fPane");
        Button btnSearch = new Button("Search");
        searchField = new TextField();
        searchField.setId("SearchField");
        searchField.setPromptText("Search terms");


        Label optionLabel = new Label("Search by:");
        optionLabel.setId("searchLabel");

        ToggleGroup group = new ToggleGroup();
        RadioButton rbAll = new RadioButton("Everything");
        RadioButton rbName = new RadioButton("File name");
        RadioButton rbLabel = new RadioButton("Label");
        RadioButton rbDate = new RadioButton("Date");

        rbAll.setToggleGroup(group);
        rbName.setToggleGroup(group);
        rbLabel.setToggleGroup(group);
        rbDate.setToggleGroup(group);

        group.selectToggle(rbAll);

        VBox radioButtons = new VBox();
        radioButtons.getChildren().addAll(rbAll, rbName, rbLabel, rbDate);
        radioButtons.setSpacing(12);
        radioButtons.setPadding(new Insets(5, 0, 5, 15));

        rbAll.setOnAction(e -> searchField.setPromptText("Search terms"));
        rbName.setOnAction(e -> searchField.setPromptText("Search file name"));
        rbLabel.setOnAction(e -> searchField.setPromptText("Search label"));
        rbDate.setOnAction(e -> searchField.setPromptText("Date created: MM/DD/YY"));

        btnSearch.setOnAction(e -> this.controller.search(searchField.getText().trim(), group.getSelectedToggle(), this.explorerPane));
        searchField.setOnAction(e -> this.controller.search(searchField.getText().trim(), group.getSelectedToggle(), this.explorerPane));

        firstLayer.getChildren().addAll(searchField, btnSearch);
        this.fPane.getChildren().clear();
        this.fPane.getChildren().addAll(title, separator, firstLayer, optionLabel, radioButtons);
        updatePane(fPane);
    }
}
