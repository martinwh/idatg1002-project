package edu.group4.views;

import edu.group4.model.ExtractTags;
import edu.group4.model.ImageDetails;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.CacheHint;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * The ImageViewPane class is responsible for displaying the selected picture,
 * also for showing the metadata of a given image.
 * <p>
 * It will display an already processed image from the Explorer pane
 * with the metadata displayed below it
 */
public class ImageViewPane {
    private final ExtractTags extractTags;
    private static ImageViewPane imageViewPane_instance = null;
    private final ScrollPane imageDetailPane;
    private VBox vBox;

    /**
     * Constructor, initializing objects
     */
    private ImageViewPane() {
        this.extractTags = new ExtractTags();
        this.imageDetailPane = new ScrollPane();
        this.imageDetailPane.setId("imageViewPane");
        this.imageDetailPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        this.vBox = new VBox();
        this.vBox.prefWidthProperty().bind(imageDetailPane.widthProperty());
        this.vBox.setId("explorerPane");
        this.vBox.setPadding(new Insets(5,5,5,10));

        this.imageDetailPane.setContent(vBox);
    }

    /**
     * Singleton design, makes sure there is only one instance of the class
     *
     * @return the only instance of the class
     */
    public static ImageViewPane getInstance() {
        if (imageViewPane_instance == null) imageViewPane_instance = new ImageViewPane();
        return imageViewPane_instance;
    }

    /**
     * Gets the pane of the application
     *
     * @return imageDetailPane, includes the GUI part of the metadata
     */
    public ScrollPane getPane() {
        return this.imageDetailPane;
    }

    /**
     * This method runs every time the image is clicked.
     * It will find the image selected, extract metadata
     * and an already process image.
     *
     * @param id             of a picture
     * @param selectedImages all selected images in a ArrayList, if selectedImages not is 1, it will not display the metadata
     * @param imagesFromDB   all images from the database
     * @param pane           includes the images
     * @param removeImage    true or false, if true it will clear the metadata.
     */
    public void viewSelectedImage(String id, ArrayList<String> selectedImages, TreeMap<String, ImageDetails> imagesFromDB, StackPane pane, boolean removeImage) {
        vBox.getChildren().clear();
        if (selectedImages.size() == 0) {
            Label label = new Label("Please select an image");
            vBox.getChildren().addAll(label);
        } else if (selectedImages.size() > 1) {
            Label selected = new Label(selectedImages.size() + " selected images");
            vBox.getChildren().addAll(selected);
        } else {
            //Get already imported image
            ImageDetails imageDetails = imagesFromDB.get(id);
            ImageView oldimageView = (ImageView) pane.getChildren().get(0);
            Image image = oldimageView.getImage();

            //Create image view.
            ImageView imageView = new ImageView(image);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
            imageView.setCacheHint(CacheHint.SPEED);
            imageView.setFitWidth(imageDetailPane.getWidth());
            imageView.setStyle("-fx-alignment: center;");
            imageView.fitWidthProperty().bind(vBox.widthProperty());
            imageView.fitHeightProperty().set(250);

            StackPane imageHolder = new StackPane();
            imageHolder.getChildren().add(imageView);
            imageHolder.setPrefHeight(250);

            //Metadata preview:
            VBox vBoxText = new VBox();
            String fullPath = imageDetails.getPath();
            int lastSlash = fullPath.lastIndexOf("\\");

            Text fileName = new Text("File name: " + imageDetails.getFileName());
            Text fileType = new Text("File type: " + imageDetails.getFileType());
            Text fileSize = new Text("File size: " + imageDetails.getFileSize());
            Text pictureResolution = new Text("Resolution: " + imageDetails.getPictureResolution());
            Text path = new Text("Path: " + fullPath.substring(0, lastSlash) + "\\");
            Text creationDate = new Text("Date created: " + imageDetails.getCreationDate());
            Text modifiedDate = new Text("Date modified: " + imageDetails.getLastModified());
            Text spacer = new Text();
            Text labels = new Text("No label added");
            if (imageDetails.getLabels().size() > 0) {
                labels = new Text("Labels: " + imageDetails.getLabels());
            }

            vBoxText.getChildren().addAll(fileName, fileType, fileSize, pictureResolution, path, creationDate, modifiedDate, spacer, labels);
            vBox.getChildren().addAll(imageHolder, vBoxText);
        }
    }
}
