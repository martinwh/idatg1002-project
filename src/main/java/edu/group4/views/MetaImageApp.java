package edu.group4.views;


import edu.group4.controllers.MainController;
import edu.group4.model.ExtractTags;
import edu.group4.model.MetaImage;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.JMetroStyleClass;
import jfxtras.styles.jmetro.Style;

import java.util.logging.Logger;

/**
 * This class handles the UI of the application.
 * It displays a menubar, feature pane, explorer pane and a detail pane.
 * It will get all the different panes and combine them into one scene displayed to the user.
 * <p>
 * The application should be able to read all the image metadata from the image and store it in a database.
 * The image itself should not be stored, only the path to it. A user should be able to search after an image based
 * on metadata or labels they have added themselves. Images should be displayed as
 * thumbnails in the application and a user should be able to create a PDF photo album of the images.
 */
public class MetaImageApp extends Application {
    private Logger logger;
    private FeaturePane featurePane;
    private ExplorerPane explorerPane;
    private ImageViewPane imageViewPane;
    private MainController mainController;
    private ExtractTags extractTags;
    private MetaImage metaImage;
    private HBox menuBar;
    private HBox appBody;
    private JMetro jMetro;

    /**
     * Initializes the application with all the different panes.
     *
     * @throws Exception Exception in super.
     */
    @Override
    public void init() throws Exception {
        super.init();

        //initiating
        this.logger = Logger.getLogger(MetaImageApp.class.getName());
        this.featurePane = FeaturePane.getInstance();
        this.explorerPane = ExplorerPane.getInstance();
        this.imageViewPane = ImageViewPane.getInstance();
        this.extractTags = new ExtractTags();
        this.mainController = new MainController();
        this.metaImage = this.mainController.loadMetaImage();

        this.mainController.loadImagesFromDB(this.metaImage, this.explorerPane);
    }

    /**
     * Creates the stage for the application, and displays the window to the user.
     * Puts all the panes together before displaying the application.
     *
     * @param primaryStage stage
     */
    @Override
    public void start(Stage primaryStage) {
        jMetro = new JMetro(Style.DARK);


        //Creating panes
        this.menuBar = new HBox();
        this.appBody = new HBox();


        //Create panes for main
        VBox root = new VBox();
        menuBar = createMenuBar();
        appBody = createAppBody();


        //Set id for panes
        root.setId("root");
        this.menuBar.setId("menuBar");
        this.appBody.setId("applicationBody");

        this.menuBar.getStyleClass().add(JMetroStyleClass.BACKGROUND);
        this.appBody.getStyleClass().add(JMetroStyleClass.BACKGROUND);


        //Add panes and buttons to parent
        root.getChildren().addAll(menuBar, appBody);

        //Enable appBody vertical scale
        VBox.setVgrow(appBody, Priority.ALWAYS);

        //Display primaryStage
        Scene scene = new Scene(root);

        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.CONTROL) {
                explorerPane.setSingleSelect(false);
            }
            if (event.getCode() == KeyCode.DELETE) {
                mainController.removeSelectedImages(metaImage, explorerPane);
            }
        });
        scene.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.CONTROL) {
                explorerPane.setSingleSelect(true);
            }
        });


        primaryStage.setTitle("MetaImage");
        jMetro.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("/style/main.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setMinWidth(500);
        primaryStage.setMinHeight(300);
        primaryStage.show();
    }

    /**
     * Stop the application
     */
    @Override
    public void stop() {
        this.metaImage.close();     // Close the database connection = logout
        System.exit(0);
    }

    /**
     * Starts the program
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Creates the menubar with buttons.
     * Defines each action when button is clicked.
     *
     * @return MenuBar as a HBox
     */
    public HBox createMenuBar() {
        //Menubar children
        ToggleGroup group = new ToggleGroup();
        ToggleButton btnMenuMetaImage = new ToggleButton("Home");
        ToggleButton btnMenuImport = new ToggleButton("Import");
        ToggleButton btnMenuCreate = new ToggleButton("Create");
        ToggleButton btnMenuSearch = new ToggleButton("Search");


        btnMenuMetaImage.setToggleGroup(group);
        btnMenuImport.setToggleGroup(group);
        btnMenuCreate.setToggleGroup(group);
        btnMenuSearch.setToggleGroup(group);

        btnMenuMetaImage.setSelected(true);

        //Make sure always one button is selected
        group.selectedToggleProperty().addListener((obsVal, oldVal, newVal) -> {
            if (newVal == null)
                oldVal.setSelected(true);
        });


        btnMenuMetaImage.setOnAction(e -> featurePane.loadMetaImage());
        btnMenuImport.setOnAction(e -> featurePane.loadImport());
        btnMenuCreate.setOnAction(e -> featurePane.loadCreate());
        btnMenuSearch.setOnAction(e -> featurePane.loadSearch());

        menuBar.getChildren().addAll(btnMenuMetaImage, btnMenuImport, btnMenuSearch, btnMenuCreate);
        return menuBar;
    }

    /**
     * Creates the application body
     *
     * @return the application body as a HBox
     */
    public HBox createAppBody() {
        //ApplicationBody children
        ScrollPane fPane = this.featurePane.getPane();
        ScrollPane iViewPane = this.explorerPane.getPane();
        ScrollPane imageDetailPane = this.imageViewPane.getPane();

        //Make body scalable
        HBox.setHgrow(fPane, Priority.ALWAYS);
        HBox.setHgrow(iViewPane, Priority.ALWAYS);
        HBox.setHgrow(imageDetailPane, Priority.ALWAYS);

        appBody.getChildren().addAll(fPane, iViewPane, imageDetailPane);
        return appBody;
    }
}
