package edu.group4.model;


import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.file.FileTypeDescriptor;
import com.drew.metadata.file.FileTypeDirectory;
import edu.group4.views.MetaImageApp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;

/**
 * The ExtractTags class is responsible for extracting the metadata of an image.
 * Some methods and functions send the metadata further to ImageDetails
 * where it is stored and accessed by among other things, the ImageViewPane class.
 *
 * @version 28.04.2020
 */
public class ExtractTags {
    private final Logger logger;
    private File file;
    private Metadata metadata;
    private BasicFileAttributes attr;

    public ExtractTags() {
        this.logger = Logger.getLogger(MetaImageApp.class.getName());
    }

    /**
     * Takes a file, extract the image metadata and returns an image.
     *
     * @param file an image file
     * @return new Image with processed metadata
     * @throws Exception Error while extracting metadata
     */
    public ImageDetails getImage(File file) throws Exception {
        setFile(file);
        return new ImageDetails(file.getPath(), file.getName(), getFileType(), getFileSize(), getPictureResolution(), getCreationDate(), getLastModified());
    }

    /**
     * Get an image from database and returns the same image with updated fields
     *
     * @param imageDetails image from database
     * @return image with updated fields
     */
    public ImageDetails updateTags(ImageDetails imageDetails) {
        try {
            setFile(new File(imageDetails.getPath()));
            imageDetails.setPictureResolution(getPictureResolution());
            imageDetails.setFileSize(getFileSize());
            imageDetails.setCreationDate(getCreationDate());
            imageDetails.setLastModified(getLastModified());
            return imageDetails;
        } catch (ImageProcessingException e) {
            logger.info("Error while processing image");
        } catch (IOException e) {
            logger.info("IO Error: File might not exist");
        } catch (Exception e) {
            logger.info("Could not get tags");
        }
        return null;
    }

    /**
     * Updated fields with new file and reads metadata.
     *
     * @param file image file
     * @throws ImageProcessingException Error processing image
     * @throws IOException Image might not exist
     */
    private void setFile(File file) throws ImageProcessingException, IOException {
        this.file = file;
        this.metadata = ImageMetadataReader.readMetadata(file);
        Path path = Paths.get(String.valueOf(file));
        this.attr = Files.readAttributes(path, BasicFileAttributes.class);
    }

    /**
     * Returns file type of an given file.
     *
     * @return file type. For example: PNG, JPEG, GIF...
     * @throws Exception Error while reading file
     */
    public String getFileType() throws Exception {
        String filetype;
        FileTypeDirectory fileTypeDirectory = metadata.getFirstDirectoryOfType(FileTypeDirectory.class);
        FileTypeDescriptor fileTypeDescriptor = new FileTypeDescriptor(fileTypeDirectory);
        filetype = fileTypeDescriptor.getDescription(1);

        if (!(filetype.equalsIgnoreCase("JPEG") || filetype.equalsIgnoreCase("PNG") || filetype.equalsIgnoreCase("GIF"))) {
            throw new Exception("Incorrect file type");
        }
        return filetype;
    }

    /**
     * Get the resolution from an image by using the metadataextractor.
     * It will scan through all the tags in the image and get the width and height.
     * it will remove the "pixels" from the string.
     *
     * @return Width x Height
     */
    public String getPictureResolution() {
        String width = "";
        String height = "";
        try {
            for (Directory directory : metadata.getDirectories()) {
                for (Tag tag : directory.getTags()) {
                    if (tag.getTagName().equals("Image Width")) {
                        width = tag.getDescription().substring(0, tag.getDescription().indexOf(' '));

                    }
                    if (tag.getTagName().equals("Image Height")) {
                        height = tag.getDescription().substring(0, tag.getDescription().indexOf(' '));
                    }
                }
            }
        } catch (StringIndexOutOfBoundsException e) {
            logger.warning("metaimage-extractor could not extract width and height. Trying again with BufferedImage");
            try {
                BufferedImage image = ImageIO.read(this.file);
                width = String.valueOf(image.getWidth());
                height = String.valueOf(image.getHeight());
            } catch (Exception e2) {
                logger.info("Could not extract image width and height");
                return "";
            }
        }
        return width + " x " + height;

    }

    /**
     * Gets the file size of the image in MB
     *
     * @return file size in KB
     */
    public String getFileSize() {
        DecimalFormat numberFormat = new DecimalFormat("0.00");
        double resultAsMB = this.file.length() * 0.00000095367432d;
        return numberFormat.format(resultAsMB) + " MB";
    }

    /**
     * Returns the creation date as a String
     *
     * @return Creation Date
     */
    public String getCreationDate() {
        FileTime date = attr.creationTime();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        return df.format(date.toMillis());
    }

    /**
     * Return last modified date as a String
     *
     * @return Last Modified Date
     */
    public String getLastModified() {
        FileTime date = attr.lastModifiedTime();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        return df.format(date.toMillis());
    }
}