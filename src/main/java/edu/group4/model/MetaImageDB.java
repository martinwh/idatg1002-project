package edu.group4.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Collection;


/**
 * Connects to a database, can either be local or remote.
 * All the persistence units are listed under META-INF/persistence.xml file under resources.
 * The local database is handled by apache derby and derbytools.
 * The remote database is a MYSQL database hosted on a server at NTNU.
 */
public class MetaImageDB implements MetaImage {
    private static MetaImage metaImage_DB_instance = null;
    private final EntityManagerFactory emf;
    private static final String PERSISTENCE_UNIT_NAME = "metaimage-pu";           //Local server
    //private static final String PERSISTENCE_UNIT_NAME = "metaimage-pu-ntnu";        //NTNU-MySql server


    /**
     * Creates an instance of MetaImageDB and
     * initiate the EntityManagerFactory with the selected persistence unit.
     */
    private MetaImageDB() {
        this.emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    public static MetaImage getInstance() {
        if (metaImage_DB_instance == null) metaImage_DB_instance = new MetaImageDB();
        return metaImage_DB_instance;
    }

    /**
     * Add a new image to the database.
     * Does NOT check if data already exist. Not its job.
     *
     * @param imageDetails image
     */
    @Override
    public void addImage(ImageDetails imageDetails) {
        EntityManager em = getEM();
        try {
            em.getTransaction().begin();
            em.persist(imageDetails);
            em.getTransaction().commit();
        } finally {
            closeEM(em);
        }
    }

    /**
     * Remove an image based on primary key.
     * @param id primary key
     */
    @Override
    public void removeImage(String id) {
        EntityManager em = getEM();
        try {
            em.getTransaction().begin();
            em.remove(em.getReference(ImageDetails.class, id));
            em.getTransaction().commit();
        } finally {
            closeEM(em);
        }
    }

    /**
     * Updates an image.
     * Could update labels.
     * @param image an image as ImageDetails
     */
    @Override
    public void updateImage(ImageDetails image) {
        EntityManager em = getEM();
        try {
            em.getTransaction().begin();
            em.merge(image);
            em.getTransaction().commit();
        } finally {
            closeEM(em);
        }
    }


    /**
     * Get all images from a database
     *
     * @return A Collection of ImageDetails
     */
    @Override
    public Collection<ImageDetails> getAllImages() {
        EntityManager em = getEM();
        try {
            return em.createQuery("SELECT i FROM ImageDetails i").getResultList();
        } finally {
            closeEM(em);
        }
    }

    /**
     * Creates an EntityManager from EntityManagerFactory.
     *
     * @return new EntityManager
     */
    private EntityManager getEM() {
        return emf.createEntityManager();
    }

    /**
     * Close the selected EntityManager gracefully if
     * its open or does exist.
     *
     * @param em
     */
    private void closeEM(EntityManager em) {
        if (em != null && em.isOpen()) em.close();
    }

    /**
     * Close the EntityManagerFactory gracefully.
     */
    @Override
    public void close() {
        emf.close();
    }

}
