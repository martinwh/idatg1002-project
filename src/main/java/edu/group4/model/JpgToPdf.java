package edu.group4.model;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import edu.group4.views.MetaImageApp;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * This class converts pictures to pdf form in the "create Album" function.
 */

public class JpgToPdf {
    /**
     * Creates a PDF from the images you selected inside the app.
     * @param selectedImages All the selected images as ImageDetails type
     * @param outputfile File where album is outputted
     */
    public void createPDFFromSelectedImages(ArrayList<String> selectedImages, File outputfile) {
        Logger logger = Logger.getLogger(MetaImageApp.class.getName());
        if (outputfile != null) {
            try {
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream(outputfile));

                document.open();
                for (String f : selectedImages) {
                    document.newPage();
                    Image imageDetails = Image.getInstance(new File(f).getAbsolutePath());
                    imageDetails.setAbsolutePosition(0, 0);
                    imageDetails.setBorderWidth(0);
                    imageDetails.scaleToFit(PageSize.A4.getWidth(), PageSize.A4.getHeight());
                    document.add(imageDetails);
                }
                document.close();
            } catch (Exception e) {
                logger.info(e.getMessage());
            }
        } else logger.info("Creation of album cancelled.");

    }

}

