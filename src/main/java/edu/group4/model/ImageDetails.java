package edu.group4.model;

import javax.persistence.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * ImageDetails holds the information of each picture that gets imported.
 */
@Entity
public class ImageDetails {
    @Id
    private String path;
    private String name;
    private String fileType;
    private String fileSize;
    private String pictureResolution;
    private String creationDate;
    private String lastModified;
    @ElementCollection      //Makes it possible to store a list on a database.
    private List<String> labels;


    /**
     * Creates an object of an image
     *
     * @param path              Path to an image
     * @param name              name of the file
     * @param creationDate      Creation date of an image
     * @param fileType          The images file type
     * @param lastModified      Last modified date of an iamge
     * @param pictureResolution Resolution W x H (500 x 500)
     * @param fileSize          filesize in MB
     */
    public ImageDetails(String path, String name, String fileType, String fileSize, String pictureResolution, String creationDate, String lastModified) {
        this.path = path;
        this.name = name;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.pictureResolution = pictureResolution;
        this.creationDate = creationDate;
        this.lastModified = lastModified;
        this.labels = new ArrayList<>();
    }

    public ImageDetails() {
    }

    /**
     * returns path location from the images.
     *
     * @return path as a string
     */

    public String getPath() {
        return path;
    }

    /**
     * returns creationDate from images.
     *
     * @return Creation Date as a string - MM/DD/YYYY
     */
    public String getCreationDate() {
        return creationDate;
    }


    /**
     * sets the creation Date.
     *
     * @param creationDate Date as a string - MM/DD/YYYY
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * returns last modified date of images.
     *
     * @return LastModified Date as a string - MM/DD/YYYY
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * sets last modified date.
     *
     * @param lastModified Date as a string - MM/DD/YYYY
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }


    /**
     * Returns all labels
     *
     * @return all the labels in a List
     */
    public List<String> getLabels() {
        return labels;
    }

    /**
     * returns fileSize of images.
     *
     * @return size of an image in KB
     */
    public String getFileSize() {
        return fileSize;
    }

    /**
     * sets the size of the image files.
     *
     * @param fileSize size of an image in KB
     */
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * returns the resolution of the picture.
     *
     * @return resolution of the picture (Width x Height)
     */
    public String getPictureResolution() {
        return pictureResolution;
    }

    /**
     * sets the picture resolution.
     *
     * @param pictureResolution resolution of the picture (Width x Height)
     */
    public void setPictureResolution(String pictureResolution) {
        this.pictureResolution = pictureResolution;
    }

    /**
     * returns format of image.
     *
     * @return file type
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * sets filetype.
     *
     * @param fileType file type as string
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * sets labels.
     *
     * @param labels labels as a list
     */
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    /**
     * returns filename.
     *
     * @return filename
     */
    public String getFileName() {
        // create object of Path
        Path path = Paths.get(getPath());

        // call getFileName() and get FileName path object
        return String.valueOf(path.getFileName());
    }


    /**
     * Returns all image details to String format.
     *
     * @return info about an image
     */
    @Override
    public String toString() {
        return "ImageDetails{" +
                ", path='" + path + '\'' +
                ", creationDate='" + creationDate + '\'' +
                ", fileType='" + fileType + '\'' +
                ", lastModified='" + lastModified + '\'' +
                ", pictureResolution='" + pictureResolution + '\'' +
                ", fileSize='" + fileSize + '\'' +
                ", labels=" + labels +
                '}';
    }
}


