package edu.group4.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * Interface for the MetaImage app.
 * The interface includes all the necessary functions to make the program function.
 */
public interface MetaImage extends Serializable {
    void addImage(ImageDetails imageDetails);

    void removeImage(String id);

    void updateImage(ImageDetails image);

    Collection<ImageDetails> getAllImages();


    void close();
}
