package edu.group4.controllers;

import edu.group4.model.*;
import edu.group4.views.ExplorerPane;
import edu.group4.views.ImageViewPane;
import edu.group4.views.MetaImageApp;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.CacheHint;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 * The MainController in the application.
 * Its handles all the actions when a button is clicked in the application.
 * The controller takes care of the link between
 * the user interface view-part, and the model in the business layer of
 * the application.
 *
 * @version 28.04.2020
 */
public class MainController {

    public MetaImage loadMetaImage() {
        return MetaImageDB.getInstance();
    }

    /**
     * Get the file that the user selected in the file explorer
     * and adds them to the database and parent.
     *
     * @param amount "Single" or "multiple" import, depending on which button is clicked
     * @param db database
     * @param parent pane where images are displayed
     */
    public void importNewFile(String amount, MetaImage db, ExplorerPane parent) {
        Logger logger = Logger.getLogger(MetaImageApp.class.getName());
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        List<File> selectedFiles = new ArrayList<>();
        if (amount.equals("single")) {
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) selectedFiles.add(selectedFile);
        }
        if (amount.equals("multiple")) {
            selectedFiles = fileChooser.showOpenMultipleDialog(null);
        }

        ExtractTags extractTags = new ExtractTags();
        if (selectedFiles != null) {
            for (File file : selectedFiles) {
                try {
                    boolean exists = false;
                    ImageDetails imageDetails = extractTags.getImage(file);
                    if (!parent.getImagesFromDB().containsKey(file.getPath())) {
                        db.addImage(imageDetails);
                        parent.addImage(imageDetails);
                    } else throw new FileAlreadyExistsException(imageDetails.getFileName() + " is already imported");
                } catch (Exception e) {
                    logger.info(e.getMessage());
                }

            }
        }
    }

    /**
     * Loads images from a database, updates fields, add them to the parent
     *
     * @param db     database
     * @param parent pane to display the newly added image
     */
    public void loadImagesFromDB(MetaImage db, ExplorerPane parent) {
        Logger logger = Logger.getLogger(MetaImageApp.class.getName());
        ExtractTags extractTags = new ExtractTags();
        Collection<ImageDetails> imageDetails = db.getAllImages();
        for (ImageDetails imageDetailsFromDB : imageDetails) {
            try {
                ImageDetails updatedImageDetails = extractTags.updateTags(imageDetailsFromDB);
                if (updatedImageDetails != null) parent.addImage(updatedImageDetails);
                else db.removeImage(imageDetailsFromDB.getPath());
            } catch (Exception e) {
                logger.info("Error loading " + imageDetailsFromDB.getPath() + " from db");
            }
        }
    }

    /**
     * This method handles the selection of images.
     * if albumMode is activated, multiple images could be selected.
     * Selected images are added to a list, selectedImages for easier access when
     * user wants to create an album.
     *
     * @param pane          pane that holds the image view
     * @param imageId       id of the image
     * @param parent        Explorer pane, where images are stored/displayed
     * @param imageViewPane Where the info about the selected image should be displayed
     */
    public void imageClicked(StackPane pane,
                             String imageId,
                             ExplorerPane parent,
                             ImageViewPane imageViewPane) {
        ArrayList<String> selectedImages = parent.getSelectedImages();
        if (parent.isSingleSelect() && !selectedImages.isEmpty()) {
            // if singleSelect is enabled and the list is not empty
            // unselect all the selected images and clear list.
            // Because only one image can be selected at a time
            // when album mode is not activated
            selectedImages.forEach(img -> parent.getImageViews().get(img).getStyleClass().remove("ExplorerPaneImgSelected"));
            selectedImages.clear();

            //Add styling to the new selected image and add to list
            pane.getStyleClass().add("ExplorerPaneImgSelected");
            selectedImages.add(imageId);
        } else {
            // Select image
            if (!selectedImages.contains(imageId)) {
                pane.getStyleClass().add("ExplorerPaneImgSelected");
                selectedImages.add(imageId);
            }
        }
        //Sends the image to the view pane to display data
        imageViewPane.viewSelectedImage(imageId, selectedImages, parent.getImagesFromDB(), pane, false);

    }

    /**
     * Method that searches after images and display them in the explorerPane
     *
     * @param searchString input from user
     * @param filter       filter to search by. Ex: Everything, Name, Label...
     * @param parent       where images are displayed
     */
    public void search(String searchString, Toggle filter, ExplorerPane parent) {
        Collection<ImageDetails> imagesFromDB = parent.getImagesFromDB().values();

        //Get selected radio button
        RadioButton selectedRadioButton = (RadioButton) filter;
        String selectedButtonValue = selectedRadioButton.getText();

        //Find results
        List<String> results = new ArrayList<>();
        if (selectedButtonValue.equals("Everything")) {
            for (ImageDetails imageDetails : imagesFromDB) {
                String creationDate = imageDetails.getCreationDate();
                if (imageDetails.getFileName().toLowerCase().contains(searchString.toLowerCase())) results.add(imageDetails.getPath());
                else if (creationDate.toLowerCase().contains(searchString.toLowerCase()))
                    results.add(imageDetails.getPath());
                else {
                    for (String label : imageDetails.getLabels()) {
                        if (label.contains(searchString)) results.add(imageDetails.getPath());
                    }
                }
            }
        }
        if (selectedButtonValue.equals("File name")) {
            for (ImageDetails imageDetails : imagesFromDB) {
                if (imageDetails.getFileName().toLowerCase().contains(searchString.toLowerCase())) results.add(imageDetails.getPath());
            }
        }
        if (selectedButtonValue.equals("Label")) {
            for (ImageDetails imageDetails : imagesFromDB) {
                for (String label : imageDetails.getLabels()) {
                    if (label.toLowerCase().startsWith(searchString.toLowerCase())) results.add(imageDetails.getPath());
                }
            }
        }
        if (selectedButtonValue.equals("Date")) {
            for (ImageDetails imageDetails : imagesFromDB) {
                String creationDate = imageDetails.getCreationDate();
                if (creationDate.toLowerCase().contains(searchString.toLowerCase()))
                    results.add(imageDetails.getPath());
            }
        }

        parent.setSearchActive(true);
        parent.displayImages(results);
    }

    /**
     * Method that displays an image in an popout window.
     * The image is in full resolution.
     *
     * @param id           PrimaryKey
     * @param imagesFromDB all the images from DB
     */
    public void imagePopOut(String id, TreeMap<String, ImageDetails> imagesFromDB) {
        ImageDetails imageDetails = imagesFromDB.get(id);
        Image newImage = new Image(Paths.get(imageDetails.getPath()).toUri().toString());
        Pane pane = new Pane();
        ImageView iv = new ImageView(newImage);
        pane.getChildren().add(iv);
        iv.setPreserveRatio(true);
        iv.setSmooth(true);
        iv.setCache(true);
        iv.setCacheHint(CacheHint.SPEED);

        Stage popUpWindow = new Stage();

        popUpWindow.initModality(Modality.APPLICATION_MODAL);
        popUpWindow.setTitle("Image View");
        VBox layout = new VBox(10);
        iv.fitWidthProperty().bind(layout.widthProperty());
        iv.fitHeightProperty().bind(layout.heightProperty());
        layout.getChildren().addAll(pane);
        layout.setAlignment(Pos.CENTER);
        Scene scene1 = new Scene(layout, 500, 500);

        popUpWindow.setScene(scene1);

        popUpWindow.showAndWait();

    }

    /**
     * Add or remove a label to an image
     *
     * @param id           PrimaryKey
     * @param imagesFromDB all the images from db
     * @param db           database
     */
    public void addLabel(String id, TreeMap<String, ImageDetails> imagesFromDB, MetaImage db) {
        ImageDetails image = imagesFromDB.get(id);
        ObservableList<String> observableList = FXCollections.observableList(image.getLabels());

        TextField newLabel = new TextField();
        Label labelNewLabel = new Label("New Label");

        Button btnAdd = new Button("Add");
        Button btnRemove = new Button("Remove");

        HBox options = new HBox();
        options.getChildren().addAll(btnAdd, btnRemove);

        Label lblAllLabels = new Label("Labels: ");
        Text txtallLabels = new Text();
        txtallLabels.setText(String.join(",", observableList));

        btnAdd.setOnAction(e -> {
            String newlabelTrimmed = newLabel.getText().trim();
            if (!newlabelTrimmed.equalsIgnoreCase("")) {
                // If label does not exist, add label
                if (observableList.stream().noneMatch(l -> l.equalsIgnoreCase(newlabelTrimmed)))
                    observableList.add(newlabelTrimmed);
            }
            newLabel.clear();
        });
        btnRemove.setOnAction(e -> {
            if (!newLabel.getText().trim().equalsIgnoreCase("")) {
                observableList.remove(newLabel.getText().trim());
                newLabel.clear();
            }
        });
        observableList.addListener((ListChangeListener<String>) c -> txtallLabels.setText(String.join(",", observableList)));

        Dialog dialog = new Dialog();
        VBox vbox = new VBox();
        vbox.getChildren().addAll(labelNewLabel, newLabel, options, lblAllLabels, txtallLabels);

        // Set the button types.
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        dialog.getDialogPane().setContent(vbox);
        dialog.showAndWait();

        //Update database
        image.setLabels(observableList);
        db.updateImage(image);
    }

    /**
     * Removes an image from explorer pane
     * Removes the processed image
     * Removes the image from DB
     * Removes the image from selectedImages
     *
     * @param db     database
     * @param parent where images are displayed
     */

    public void removeSelectedImages(MetaImage db, ExplorerPane parent) {
        ArrayList<String> selectedImages = parent.getSelectedImages();
        TreeMap<String, StackPane> imageViews = parent.getImageViews();
        TreeMap<String, ImageDetails> imagesFromDB = parent.getImagesFromDB();
        FlowPane displayedImages = parent.getFlowPane();

        selectedImages.forEach(id -> {
            db.removeImage(id);
            displayedImages.getChildren().remove(imageViews.get(id));
            imageViews.remove(id);
            imagesFromDB.remove(id);
        });
        selectedImages.clear();
        ImageViewPane.getInstance().viewSelectedImage(null, selectedImages, imagesFromDB, null, true);
    }


    /**
     * Creates a PDF of all the selected images.
     * Select where you want to save the file.
     * You can also select name.
     *
     * @param selectedImages Images that are selected as ImageDetails
     */
    public void createPDF(ArrayList<String> selectedImages) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Album");
        fileChooser.setInitialFileName("Album");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("pdf", "*.pdf")
        );

        File file = fileChooser.showSaveDialog(null);


        JpgToPdf jpgPDF = new JpgToPdf();
        jpgPDF.createPDFFromSelectedImages(selectedImages, file);
    }
}
