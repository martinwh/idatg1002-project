/**
 * The MainController in the application.
 * Its handles all the actions when a button is clicked in the application.
 * The controller takes care of the link between
 * the user interface view-part, and the model in the business layer of
 * the application.
 *
 * @version 28.04.2020
 */
package edu.group4.controllers;