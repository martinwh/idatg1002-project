package edu.group4.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class testes the functionally of the database.
 * This class is dependent on the class ExtractTags so that need to be tested first.
 *
 * NOTE: Delete database before running test class! Change persistence unit to local database first in MetaImageDB.
 *
 *
 * */
class MetaImageDBTest {
    private MetaImage metaImageDB;

    @BeforeEach
    void setUp() {
        this.metaImageDB = MetaImageDB.getInstance();
    }

    /**
     * Testing adding and removing an image
     * Also testing get all images
     * Also updating an image with labels
     */
    @Test
    void addAndRemoveImage() throws Exception {
        ExtractTags extractTags = new ExtractTags();

        //Get all images from db, update tags
        ArrayList<ImageDetails> imagesFromDB = new ArrayList<>();
        for (ImageDetails image : metaImageDB.getAllImages()) {
            ImageDetails imageFromDB = extractTags.updateTags(image);
            imagesFromDB.add(imageFromDB);
        }

        //Create new image and add to database
        ImageDetails newImage = extractTags.getImage(new File("src/test/resources/andy-li-aRs9EUXfrKQ-unsplash.jpg"));
        metaImageDB.addImage(newImage);

        //Get image from db
        System.out.println("Test nr 1: Test if an image is added to a database");
        for (ImageDetails i : imagesFromDB) {
            if (!(i.getPath().equalsIgnoreCase(newImage.getPath()))) fail("Image not imported");
        }

        System.out.println("Test nr 2: Check that image have no labels");
        assertTrue(newImage.getLabels().isEmpty());

        System.out.println("Test nr 3: Add labels to image");
        ArrayList<String> newLabels = new ArrayList<>();
        newLabels.add("Label1");
        newLabels.add("Label2");
        newLabels.add("Label2 22");
        newImage.setLabels(newLabels);
        assertEquals(newImage.getLabels().size(), newLabels.size());
        assertTrue(newImage.getLabels().contains("Label1"));
        assertTrue(newImage.getLabels().contains("Label2"));
        assertTrue(newImage.getLabels().contains("Label2 22"));

        System.out.println("Test nr 4: update database with image with label");
        metaImageDB.updateImage(newImage);

        //Get updated database
        ImageDetails updatedImageFromDB = null;
        for (ImageDetails image : metaImageDB.getAllImages()) {
            if (image.getPath().equalsIgnoreCase(newImage.getPath())) updatedImageFromDB = extractTags.updateTags(image);
            if (!(image.getPath().equalsIgnoreCase(newImage.getPath()))) fail("Image not saved in database");
        }
        assertEquals(newImage.getLabels().size(), newLabels.size());
        assertTrue(newImage.getLabels().contains("Label1"));
        assertTrue(newImage.getLabels().contains("Label2"));
        assertTrue(newImage.getLabels().contains("Label2 22"));


        System.out.println("Test nr 5: Remove image from DB");
        metaImageDB.removeImage(newImage.getPath());
        for (ImageDetails image : metaImageDB.getAllImages()) {
            assert updatedImageFromDB != null;
            if (image.getPath().equalsIgnoreCase(updatedImageFromDB.getPath())) fail("Image not deleted");
        }


    }

}