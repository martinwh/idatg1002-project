package edu.group4.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class testes everything that handles adding or updating an image.
 *
 * NOTE: Check input date's manual in getTags()
 */
class ExtractTagsTest {
    private ExtractTags extractTags;
    private File testRandomFile;
    private ImageDetails imageJPG;
    private ImageDetails imagePNG;
    private ImageDetails imageGIF;

    /**
     * Initiates the tests with known images.
     * @throws Exception File might not exist
     */
    @BeforeEach
    void setUp() throws Exception {
        this.extractTags = new ExtractTags();
        this.testRandomFile = new File("src/test/resources/Categories.docx");
        this.imageJPG = extractTags.getImage(new File("src/test/resources/andy-li-aRs9EUXfrKQ-unsplash.jpg"));
        this.imagePNG = extractTags.getImage(new File("src/test/resources/anaconda_PNG15.png"));
        this.imageGIF = extractTags.getImage(new File("src/test/resources/giphy.gif"));
    }

    /**
     * Test if getImage only works on the filetypes supported:
     * ".png", "*.jpg", "*.gif"
     */
    @Test
    void getImage() {
        System.out.println("Test nr 1: getImage returns ImageDetails with an image of type jpg");
        assertNotNull(imageJPG);

        System.out.println("Test nr 2: getImage returns ImageDetails with an image of type png");
        assertNotNull(imagePNG);

        System.out.println("Test nr 3: getImage returns ImageDetails with an image of type gif");
        assertNotNull(imageGIF);

        try {
            System.out.println("Test nr 4: getImage returns exception when adding random file");
            ImageDetails randomFile = extractTags.getImage(testRandomFile);
            fail("Exception not thrown");
        } catch (Exception e) {
            //System.out.println(e.getMessage());
            assertEquals("Incorrect file type", e.getMessage());
        }
    }

    /**
     * Test if getTags updates the metadata.
     * This also tests methods:
     * - getCreationDate()
     * - getLastModified()
     *
     *  NOTE: Check dates manually and input in assertEquals.
     */
    @Test
    void getTags() {
        System.out.println("Test nr 5: Verify that dates are correct");
        // Input dates manually here:
        assertEquals("04/28/2020", imageJPG.getCreationDate());
        assertEquals("04/29/2020", imageJPG.getLastModified());

        System.out.println("Test nr 6: Change dates manually");
        imageJPG.setCreationDate("05/01/2020");
        imageJPG.setLastModified("01/29/2020");
        assertEquals("05/01/2020", imageJPG.getCreationDate());
        assertEquals("01/29/2020", imageJPG.getLastModified());


        System.out.println("Test nr 7: get updated/real dates");
        ImageDetails updatedImage = extractTags.updateTags(imageJPG);
        // Input dates manually here:
        assertEquals("04/28/2020", updatedImage.getCreationDate());
        assertEquals("04/29/2020", updatedImage.getLastModified());
    }

    /**
     * Checks if it returns the correct file type.
     */
    @Test
    void getFileType() {
        System.out.println("Test nr 8: Test File type: JPEG");
        assertTrue(imageJPG.getFileType().equalsIgnoreCase("jpeg"));

        System.out.println("Test nr 9: Test File type: PNG");
        assertTrue(imagePNG.getFileType().equalsIgnoreCase("png"));

        System.out.println("Test nr 10: Test File type: GIF");
        assertTrue(imageGIF.getFileType().equalsIgnoreCase("gif"));
    }

    @Test
    void getPictureResolution() {
        System.out.println("Test nr 11: Get resolution from JPEG filetype");
        assertTrue(imageJPG.getPictureResolution().equalsIgnoreCase("6000 x 4000"));

        System.out.println("Test nr 12: Get resolution from PNG filetype");
        assertTrue(imagePNG.getPictureResolution().equalsIgnoreCase("647 x 720"));

        System.out.println("Test nr 13: Get resolution from GIF filetype");
        assertTrue(imageGIF.getPictureResolution().equalsIgnoreCase("320 x 240"));
    }

    @Test
    void getFileSize() {
    }
}