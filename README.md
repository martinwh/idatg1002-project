# IDATG1002 2020 Group 4
### Git Flow
The project was first based on the git flow where you have a master branch for stable releases and a developing branch with most up to date features. When creating features you branch out from master, same with hotfixes. We tried to do the same here, so our main branch is **develop** which is the most up to date branch. Our wiki is also connected with the develop branch, but not the JavaDoc.

**JavaDoc:** http://martinwh.pages.stud.idi.ntnu.no/idatg1002-group4/

### Assignment description
The task in this assignment was to create an imaging application where a user should be able to register images. The application should be able to read all the image metadata from the image and store it in a database. The image itself should not be stored, only the path to it. A user should be able to search after an image based on metadata or labels they have added themselves. Images should be displayed as thumbnails in the application and a user should be able to create a PDF photo album of the images.

### Group members
* Eilert Tunheim 
* Eirik Norbye 
* Erlend Johan Vannebo 
* Martin Wighus Holtmon 
* Martin Johansen 

### Dependencies
To run the applicaiton:
* Java Runtime Environment

To compile the applicaiton:
* Java 8
* JavaFX 11.0.2
